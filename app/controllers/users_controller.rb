class UsersController < ApplicationController
  def index
    render :json => User.all
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      render :json => @user
    else
      render :json => @user.errors.full_messages, :status => :unprocessable_entity
    end
  end

  def show
    render :json => User.find(params[:id])
  end

  def update
    @user_to_update = User.find(params[:id])

    if @user_to_update.update_attributes(params[:user]) == false
      render :json => @user_to_update.errors.full_messages
    else
      render :json => @user_to_update
    end
  end

  def destroy
    @user = User.find(params[:id])
    render :json => @user
    @user.destroy
  end
end
