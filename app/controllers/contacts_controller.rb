class ContactsController < ApplicationController
  def index
    if params.has_key?(:user_id)
      # index of nested resource
      @contacts = Contact.contacts_for_user_id(params[:user_id])
      render :json => @contacts
    end

  end

  def create
    @contact = Contact.new(params[:contact])
    if @contact.save
      render :json => @contact
    else
      render :json => @contact.errors.full_messages, :status => :unprocessable_entity
    end
  end

  def show
    render :json => Contact.find(params[:id])
  end

  def update
    @contact_to_update = Contact.find(params[:id])

    if @contact_to_update.update_attributes(params[:contact]) == false
      render :json => @contact_to_update.errors.full_messages
    else
      render :json => @contact_to_update
    end
  end

  def destroy
    @contact = Contact.find(params[:id])
    render :json => @contact
    @contact.destroy
  end
end
