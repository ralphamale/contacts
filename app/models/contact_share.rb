class ContactShare < ActiveRecord::Base
  attr_accessible :contact_id, :user_id

  validates :contact_id, :user_id, :presence => true
  # validates :contact_id, scope: :user_id, uniqueness: true

  belongs_to :user

  belongs_to :contact
end
