class Contact < ActiveRecord::Base
  attr_accessible :name, :email, :user_id
  validates :name, :email, :user_id, presence: true
  validates :email, uniqueness: true

  belongs_to :owner,
  :class_name => "User",
  :foreign_key => :user_id

  has_many :contact_shares

  has_many :shared_users,
  :through => :contact_shares,
  :source => :user

  def self.contacts_for_user_id(user_id)
    sql = <<-SQL
    SELECT contacts.*

    FROM contacts
    LEFT OUTER JOIN contact_shares
    ON contacts.id = contact_shares.contact_id

    WHERE contacts.user_id = ?
    OR contact_shares.user_id = ?
    SQL

    contacts = Contact.find_by_sql([sql, user_id, user_id])
  end
end
