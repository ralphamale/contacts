class InitialTables < ActiveRecord::Migration
  def change

    drop_table :users

    create_table :users do |t|
      t.string :username, :unique => true, :null => false

      t.timestamps
    end

    create_table :contacts do |t|
      t.string :name, :null => false
      t.string :email, :null => false, :unique => true
      t.references :user, :null => false

      t.timestamps
    end

    add_index :contacts, :user_id

    create_table :contact_shares do |t|
      t.references :contact, :null => false
      t.references :user, :null => false
    end

    add_index :contact_shares, :contact_id
    add_index :contact_shares, :user_id

    # add_index :contact_shares, [:contact_id, :user_id], unique: true

  end
end
